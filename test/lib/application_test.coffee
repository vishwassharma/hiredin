application = require 'lib/application'

describe "[Lib] Application Test", ->

	it "should be a object", ->
		expect(_.isObject application).to.equal true

	it "should have function initRouter" , ->
		expect(application.initRouter).to.be.a('function')

	it "should have function initLayout", ->
		expect(application.initLayout).to.be.a('function')

	it "should have function initController", ->
		expect(application.initController).to.be.a('function')

	it "should have function getControllerByName", ->
		expect(application.getControllerByName).to.be.a('function')

	describe "Controller Related Function", ->

		it "should cache controllers", ->
			expect(application.controllers).to.be.a('array')
			expect(application.controllerByName).to.be.a('object')

		describe "#addController function", ->

			it "should fails when arguments are not given", ->
				# expect(application.addController()).to.throw
				# expect(false).to.equal(true)

			it "should take check arguments sanity" , ->
				# expect(false).to.equal(true)

		describe "#getControllerByName function", ->

			it "should fails when arguments are not given", ->
				# expect(application.addController()).to.throw
				# expect(false).to.equal(true)

			it "should take check arguments sanity" , ->
				# expect(false).to.equal(true)