Subscriber = require 'lib/subscriber'
mediator = require 'lib/mediator'

describe "[LIB] Subscriber", ->

	it "should be exported object", ->
		expect(_.isObject Subscriber).to.equal true

	it "should have a subscribeEvent function" , ->
		expect(Subscriber.subscribeEvent).to.be.a('function')

	it "should have unsubscribeEvent function", ->
		expect(Subscriber.unsubscribeEvent).to.be.a('function')

	it "should have an unsubscribeAllEvents function", ->
		expect(Subscriber.unsubscribeAllEvents).to.be.a('function')

	it "should check for subscriberEvent arguments type and throw error", ->
		( ->
			Subscriber.subscribeEvent(1)
		).should.throw("1st argument should be a string")

		( ->
			Subscriber.subscribeEvent("1", "2")
		).should.throw("2st argument should be a callback function")

	it "should be able subscribe a event", ->
		spy = sinon.spy()
		# subscibe event
		Subscriber.subscribeEvent("place", spy)
		spy.should.not.have.been.calledOnce
		mediator.publish "place"
		spy.should.have.been.calledOnce

	it "should check for unsubscriberEvent arguments type and throw error", ->
		( ->
			Subscriber.unsubscribeEvent(1)
		).should.throw("1st argument should be a string")

		( ->
			Subscriber.unsubscribeEvent("1", "2")
		).should.throw("2st argument should be a callback function")

	it "should be able unsubscribe a event", ->
		spy = sinon.spy()
		# subscibe event
		Subscriber.subscribeEvent("place", spy)
		spy.should.not.have.been.calledOnce
		mediator.publish "place"
		spy.should.have.been.calledOnce
		# second call
		Subscriber.unsubscribeEvent "place", spy
		mediator.publish "place"
		spy.should.not.have.been.calledTwice

	it "should be able unsubscribe all events", ->
		spy = sinon.spy()
		# subscibe event
		Subscriber.subscribeEvent("place", spy)
		spy.should.not.have.been.calledOnce
		mediator.publish "place"
		spy.should.have.been.calledOnce
		# second call
		Subscriber.unsubscribeAllEvents()
		mediator.publish "place"

		spy.should.not.have.been.calledTwice