Collection = require 'lib/models/collection'
Subscriber = require 'lib/subscriber'

describe "[LIB] Collection", ->

	collect = null

	beforeEach ->
		collect = new Collection

	afterEach ->
		# collect.dispose()

	it "should be exported object", ->
		expect(_.isObject Collection).to.equal true


	it "should mixing subscriber", ->
		for own name, value of Subscriber
			expect(collect[name]).to.equal Subscriber[name]