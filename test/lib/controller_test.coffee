Controller = require 'lib/controllers/controller'
Subscriber = require 'lib/subscriber'

describe "[LIB] Controller", ->

	cont = null

	beforeEach ->
		cont = new Controller

	afterEach ->
		cont.dispose()
		cont = null

	it "should be exported object", ->
		expect(_.isObject Controller).to.equal true

	it "should mixing subscriber", ->
		for own name, value of Subscriber
			expect(cont[name]).to.equal Subscriber[name]

	it "should have a disposed function dispose", ->
		# check that if it is already not disposed
		expect(cont.disposed).to.equal false
		# check if dispose is a function somewhere
		expect(cont.dispose).to.be.a 'function'

	it "should be have the ability to dispose itself", ->
		# try to dispose
		cont.dispose()

		# if dispose is successful then we will have disposed to be true
		cont.disposed.should.be.equal true