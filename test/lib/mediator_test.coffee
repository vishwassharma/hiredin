mediator = require 'lib/mediator'

describe "[Lib] Mediator Test", ->

	it "should be defined" , ->
		expect(_.isObject mediator).to.equal true
		expect(mediator).to.exist

	# it "should have active buttom", ->
	# 	expect(mediator.isActive).to.equal(true)

	it "should have subscribe, unsubscribe, and publish functions" , ->
		expect(mediator.subscribe).to.be.a('function')
		expect(mediator.unsubscribe).to.be.a('function')
		expect(mediator.publish).to.be.a('function')

	it "should have supports for channels", ->
		expect(mediator.channels).to.not.be.undefined


	describe "Publisher Test", ->

		eventName = 'foo'
		payload = 'payload'
		spy = null

		beforeEach ->
			spy = sinon.spy()

		afterEach ->
			delete spy

		it 'should publish messages to subscribers', ->
			spy.should.not.have.been.called
			mediator.subscribe eventName, spy
			mediator.publish eventName
			spy.should.have.been.calledOnce

		it "should not do callback once unsubscribed", ->
			mediator.subscribe eventName, spy
			# once unsubscribed do not call
			mediator.unsubscribe eventName, spy
			mediator.publish eventName
			spy.should.not.have.been.called

		it "should unsubscribe all if callback arguments is not given", ->
			spy2 = sinon.spy()

			mediator.subscribe eventName, spy
			mediator.subscribe eventName, spy2

			mediator.unsubscribe eventName
			mediator.publish eventName
			
			spy.should.not.have.been.called
			spy2.should.not.have.been.called

		it "should call with payload", ->
			mediator.subscribe eventName, spy
			mediator.publish eventName, payload
			spy.should.have.been.calledWith payload
			mediator.unsubscribe eventName