View = require 'lib/views/view'
Subscriber = require 'lib/subscriber'

describe "[LIB] View", ->

	view = null

	beforeEach ->
		view = new View

	afterEach ->
		view.dispose()
	
	it "should be defined" , ->
		expect(_.isObject View).to.equal true
		expect(view).to.exist

	it "should have some default function", ->
		expect(view.render).to.be.a('function')
		expect(view.addSubView).to.be.a('function')
		expect(view.getTemplateData).to.be.a('function')
		expect(view.template).to.be.a('function')
		expect(view.beforeRender).to.be.a('function')
		expect(view.afterRender).to.be.a('function')
		expect(view.delegate).to.be.a('function')
		expect(view.undelegate).to.be.a('function')
		expect(view.modelBind).to.be.a('function')
		expect(view.modelUnbind).to.be.a('function')
		expect(view.modelUnbindAll).to.be.a('function')
		expect(view.dispose).to.be.a('function')

	it "should have some default options", ->
		expect(view.containerMethod).to.be.a('string')
		expect(view.container).not.to.exist
		expect(view.filter).to.be.a('boolean')
		expect(view.subViewsByName).not.to.exist
		expect(view.subViews).not.to.exist
		expect(view.hasSubView).to.be.a('boolean')
		expect(view.autoRender).to.be.a('boolean')

	it "should have some default value", ->
		expect(view.containerMethod).to.equal 'append'
		expect(view.hasSubView).to.equal false
		expect(view.autoRender).to.equal false
		expect(view.filter).to.equal false

	it "should have unsubscribeEvent function", ->
		expect(view.unsubscribeEvent).to.be.a('function')

	it "should mixing subscriber", ->
		for own name, value of Subscriber
			expect(view[name]).to.equal Subscriber[name]