Model = require 'lib/models/model'
Subscriber = require 'lib/subscriber'

describe "[LIB] Model", ->

	model = null

	beforeEach ->
		model = new Model

	afterEach ->
		# model.dispose()

	it "should be exported object", ->
		expect(_.isObject Model).to.equal true

	it "should mixing subscriber", ->
		for own name, value of Subscriber
			expect(model[name]).to.equal Subscriber[name]