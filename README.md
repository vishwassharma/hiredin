# Hiredin Application

##Modules (client side)

1. [Mediator][1]  
This module will provide pub/sub implementation. In future releases it will also provide interface for dom selectors, plugins, etc
		
		publish : do publish  
		subscribe : do subscribe
		unsubscribe : do unsubscribe
		
1. [Application][2]  
Initialize the application and provide some use ful function which will be extended to the App object.

		initRouter : This function will initialize the router used for this application
		initController : This will initialize all the controllers which will exist in all views for example header, footer, breadcrum navigation etc..

1. [Subscriber][3]  
This module will be extended in other application and will provide a mechanism of subscribing mediator events to other objects
		
		subscribeEvent : Subscirbe events  
		unsubscribeEvent : Unsubscirbe event
		unsubscribeAllEvents : Unsubscribe all events

1. [Controller][4]  
This module will be extended in other application and will provide a mechanism of subscribing mediator events to other objects. This module will help me in differentiating Model with View.

1. [Layout][5]  
The layout module will would listen on global event on controllers and do magical things. I have done just the scaffolding right now.
		
1. [View][6]  
This module will be extended in other application and will provide a mechanism of subscribing mediator events to other objects

		subscribeEvent : Subscirbe events  
		unsubscribeEvent : Unsubscirbe event
		unsubscribeAllEvents : Unsubscribe all events

1. [CollectionView][7]  
This module will provide for view rendering if we have a collection on which we would be working on.

		subscribeEvent : Subscirbe events  
		unsubscribeEvent : Unsubscirbe event
		unsubscribeAllEvents : Unsubscribe all events

1. [Model][8]  
This module will provide a lot of function that will ease the model extraction

		subscribeEvent : Subscirbe events  
		unsubscribeEvent : Unsubscirbe event
		unsubscribeAllEvents : Unsubscribe all events

1. [Collection][9]  
This Module will connect with URI and will bind data between application view and various models that it represent

		subscribeEvent : Subscirbe events  
		unsubscribeEvent : Unsubscirbe event
		unsubscribeAllEvents : Unsubscribe all events

##Version

### 0.1
* `oauth` features should be implemented
* redirect on login
* different page for session user with complete application
* checking authentication 
* should have session store
* it is good base for [brunchup][]

### 0.2
* Subscriber Module
* Controller Module 
* Layout Module
* View Module
* Collection View Module

### 0.3
* Model Module
* Collection Module

### 0.4
* Fast app layout basic .. implemented
#### 0.4.1
* Fixed Test

### 0.5


### 0.x
* Implement Handlebars Helpers
* Implement test of all the modules
* should have independent middleware
* implmenetation of mongoose-auth
* Mixins Modules
* In mixin implement datastructure
* In mixin implement URI manupulation lib



[1]: http://example.com/  "Mediator (chaplinjs) / (backbone-aura) / (blog)"
[2]: http://other.com/  "Application implementation "
[3]: http://other.com/  "Subscriber Event"
[4]: http://other.com/  "Controller Event"
[5]: http://other.com/  "Layout Event"
[6]: http://other.com/  "View Event"
[7]: http://other.com/  "Collection View Event"
[8]: http://other.com/  "Model"
[9]: http://other.com/  "Collection"

[brunchup]: http://github.com/vishwassharma/brunchup  "Brunch up project"