Controller = require 'lib/controllers/controller'
HeaderView = require "views/header_view"

class HeaderController extends Controller

	initialize : (options) ->
		super
		@view = new HeaderView

module.exports = HeaderController