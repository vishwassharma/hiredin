Controller = require 'lib/controllers/controller'
BreadCrumbView = require "views/breadcrumb_view"

class BreadCrumbController extends Controller

	initialize : (options) ->
		super
		@view = new BreadCrumbView

module.exports = BreadCrumbController