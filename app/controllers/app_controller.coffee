Controller = require 'lib/controllers/controller'
AppView = require "views/app_view"

class AppController extends Controller

	initialize : (options) ->
		super
		@view = new AppView

module.exports = AppController