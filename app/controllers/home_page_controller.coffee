Controller = require 'lib/controllers/controller'
HomePageView = require "views/home_page_view"

class HeaderController extends Controller

	view : ''

	initialize : (options) ->
		super
		@subscribeEvent 'home', @main
		
	main : (options) ->
		@view = new HomePageView

module.exports = HeaderController