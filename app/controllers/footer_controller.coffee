Controller = require 'lib/controllers/controller'
FooterView = require "views/footer_view"

class FooterController extends Controller

	initialize : (options) ->
		super
		@view = new FooterView

module.exports = FooterController