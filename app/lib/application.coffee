Application =

	layout : ''
	controllers : []
	controllerByName : {}


	# the init Router function
	initRouter : ->
		console.log "init router"

	initLayout : ->
		console.log "Init Layout"

	initController : ->
		console.log "init controller"

	initMediator : ->
		console.log "init Mediator"

	# function : addController
	# Add a new controller to the application
	# @type name - string
	# name : a unique name given to each container
	# @type controller - instanceof Controller
	# controller : a instance of controller to add into application
	# true if success false if failure due to any reason
	addController : (name, controller) ->
		true

	# function : getControllerByName
	# Get the controller 
	getControllerByName : (name) ->
		# controller = _.pluck @controllerByName, name
		# controller


module.exports = Application