Form = 
	
	# Look at SWFUpload.org for any better implementation
	formId : ""
	updateOn : ''

	params : () ->
		console.log "Here"
		$(@formId).serializeArray()

	data : ''

	bindCollection : (collection) ->
		@data = collection

	bindModel : (model) ->
		@data = model


	validate : (event) ->
		event.preventDefault() 
		# console.log event
		data = @formId.serializeArray()
		result = {}
		_.each data, (d) ->
			result[d['name']] = d['value']
		# result 
		@data.create result
		# @resetForm()

	onSubmit : (event) ->
		console.log "On submit"
		event.preventDefault()
		debugger

	# reset form
	resetForm : () ->
		# form = @formId
		console.log "Resetting form"

	update : (view) ->
		return unless view
		collection = view.collection
		collection.trigger('reset')

module.exports = Form