
# Base class for all collections.
Subscriber = require 'lib/subscriber'

module.exports = class Collection extends Backbone.Collection

	# Mixin for mediator
	_.extend @::, Subscriber