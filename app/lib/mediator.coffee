mediator = {}

mediator.channels = {}
# describe the interface for the application
mediator.subscribe = mediator.on = Backbone.Events.on
mediator.publish = mediator.trigger = Backbone.Events.trigger
mediator.unsubscribe = mediator.off = Backbone.Events.off

# all these should be read only
module.exports = mediator