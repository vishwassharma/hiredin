mediator = require 'lib/mediator'

Subscriber =

	subscribeEvent : (type, handler) ->
		if typeof type isnt 'string'
			throw new TypeError "1st argument should be a string"

		if typeof handler isnt 'function'
			throw new TypeError "2st argument should be a callback function"

		# Ensure that a handler isnt registered twice
		mediator.unsubscribe type, handler, @

		# Register global handler, force context to be subscriber
		mediator.subscribe type, handler, @


	unsubscribeEvent : (type, handler) ->
		if typeof type isnt 'string'
			throw new TypeError "1st argument should be a string"

		if typeof handler isnt 'function'
			throw new TypeError "2st argument should be a callback function"

		# Remove the event
		mediator.unsubscribe type, handler, @

	unsubscribeAllEvents : ->
		# unsubscribe all the event handler
		mediator.unsubscribe null, null, @

# You’re frozen when your heart’s not open
Object.freeze? Subscriber

module.exports = Subscriber