View = require 'lib/views/view'
template = require 'views/templates/header'

class HeaderView extends View
	template: template
	autoRender : true
	container: "#main"
	className : "navbar"
	id : "header"
	# containerMethod : 'append'

module.exports = HeaderView