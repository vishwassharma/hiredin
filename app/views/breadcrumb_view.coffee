View = require 'lib/views/view'
template = require 'views/templates/breadcrumb'

module.exports = class BreadcrumbView extends View
	template: template
	container : '#main'
	id : "breadcrumb"
	className : "row"
	autoRender : yes