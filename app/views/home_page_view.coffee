View = require 'lib/views/view'
Collection = require 'lib/models/collection'

Form = require 'lib/mixins/form'
template = require 'views/templates/home_page'

module.exports = class HomePageView extends View
	template: template
	autoRender : yes
	container : '#app'
	className : 'span12'
	id : 'home-page'
	hasSubView : yes

	_.extend @::, Form

	initialize : (options) ->
		super
		@formId = @$("#addJob")

		@delegate 'click', '#submit_btn', @validate
		@delegate 'submit', '#addJob', @onSubmit
		
		# add sub view
		@addJobView()

		# @$('#myModal').modal()
		

	addJobView : () ->
		JobsView = require "views/home/jobs"

		collection = new Collection()
		collection.model = require 'models/job'
		collection.url = "/jobs"
		
		# bindCollectionWith form
		@bindCollection(collection)

		options =
			collection : collection
			container : '#jobsview'

		@jobsView = @addSubView 'jobsView', JobsView, options

		collection.fetch()