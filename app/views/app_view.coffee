View = require 'lib/views/view'
template = require 'views/templates/app'

module.exports = class AppView extends View
	template: template
	autoRender : yes
	container : "#main"
	id : "app"
	className: "row"