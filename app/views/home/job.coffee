View = require 'lib/views/view'
template = require 'views/templates/home/job'

module.exports = class JobView extends View
	template: template
	tagName : 'li'
	className : 'row'