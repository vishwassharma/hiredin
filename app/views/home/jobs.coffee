CollectionView = require 'lib/views/collection_view'
template = require 'views/templates/home/jobs'
JobView = require 'views/home/job'

module.exports = class JobsView extends CollectionView
	template: template
	tagName : 'div'
	listSelector: '#jobs'
	className : "span7"
	itemView : JobView