View = require 'lib/views/view'
template = require 'views/templates/footer'

module.exports = class FooterView extends View
	template: template
	container : "#main"
	id : "footer"
	className : "row"
	autoRender : true