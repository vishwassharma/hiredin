Router = require 'lib/router'
HomePageController = require 'controllers/home_page_controller'
mediator = require 'lib/mediator'

module.exports = class AppRouter extends Router

	initialize : ->
		# do something about the application
		@homePage = new HomePageController

	routes:
		'': 'home'

	home: ->
		# $('body').html application.homeView.render().el
		console.log "HomePage"
		mediator.publish 'home'