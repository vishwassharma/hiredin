App = require 'lib/application'
Router = require 'router'
# Layout = require 'views/layout'


# Global controller
HeaderController = require 'controllers/header_controller'
BreadCrumbController = require 'controllers/breadcrumb_controller'
AppController = require 'controllers/app_controller'
FooterController = require 'controllers/footer_controller'

class Application

    # Extend the application
    _.extend @prototype, App

    # set the default application status for debugging
    debug : false

    constructor : ->
        @initialize arguments...

    initialize : (options) ->
        
        # check if has debug
        if _.has options, "debug"
            @debug = options.debug

        # Initialize the mediator
        @initMediator()

        # Initialize Controller
        @initController()

        # Initialize the router
        @initRouter()

        # initialize the layout
        # @initLayout()

        # initialize controllers
        # Freeze the application instance to prevent further changes
        Object.freeze? this

    # Overriding the initRouter method
    initRouter : (options) ->
        @router = new Router()

    # Layout
    initLayout : (options) ->
        @layout = new Layout()

    # Mediator
    initMediator : (options) ->
        # do something with mediator

    initController : (options) ->
        # do something about controller
        # Initialize global level controllers like header
        new HeaderController()
        new BreadCrumbController()
        new AppController()
        new FooterController()

module.exports = Application