express = require 'express'
gzippo = require 'gzippo'
sysPath = require 'path'
everyauth = require 'everyauth'
settings = require './setting'

hbs = require('hbs')

everyauth.twitter
  .consumerKey(settings.twitter.appId)
  .consumerSecret(settings.twitter.secret)
  .findOrCreateUser( (session, accessToken, accessTokenSecret, twitterUserMetadata)->
    promise = new @Promise
    promise.fulfill twitterUserMetadata
    promise
  ).redirectPath '/#callback'

everyauth.linkedin
  .consumerKey(settings.linkedin.appId)
  .consumerSecret(settings.linkedin.secret)
  .findOrCreateUser((sessions, token, secretToken, linkedinData) ->
    promise = new @Promise
    promise.fulfill linkedinData
    promise
  ).redirectPath('/#callback')

# make a mongoose connection
mongoose = require 'mongoose'
mongoose.connect settings.db
everyauth = require 'everyauth'

# get the global variables here
router = require './router'

MemStore = express.session.MemoryStore

port = process.env['app_port'] || 3000
path = 'public'
app_root =  __dirname

# Starting the server here
server = express.createServer()

# declare middle ware here
server.configure ->
  #server.use express.logger()
  server.use express.bodyParser()
  #server.use express.methodOverride()
  server.use express.cookieParser()
  server.use express.session
    secret : 'adofi02394lk324j2lkjlsdfjlsdj09329jklsdajlfd'
    #store : new MemStore
      #reapInterval : 50000 * 10
  server.use everyauth.middleware()
  server.use express.csrf()
  #server.use server.router
  server.use everyauth.middleware()
  server.set 'views', sysPath.join(app_root, 'public')
  #app.set 'view engine', 'jade'
  server.set 'view options', layout : false
  #server.use express.static sysPath.join(app_root, 'public')
  #server.use express.static sysPath.join(app_root, 'public')
  server.use gzippo.staticGzip(__dirname + '/public')
  server.use gzippo.compress()

  server.use (req, response, next) ->
    response.header 'Cache-Control', 'no-cache'
    next()
  
server.register '.html',
  compile : (str, options) ->
    (locals) ->
      str

server.get '/', router.index
#server.get '/login', router.login
server.get '/api/jobs', router.jobs
server.get '/api/seekers', router.seekers

server.all "/*", (request, response) ->
  response.sendfile sysPath.join path, '404.html'

everyauth.helpExpress(server)

server.listen port, () ->
  console.log "Listening on #{port}"