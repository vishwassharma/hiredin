express = require 'express'
sysPath = require 'path'
everyauth = require 'everyauth'
settings = require './setting'
middleware = require './middleware'

resource = require './lib/resources'

# Twitter
everyauth.twitter
	.consumerKey(settings.twitter.appId)
	.consumerSecret(settings.twitter.secret)
	.findOrCreateUser( (session, accessToke, accessTokenSecret, twitUserData) ->
		promise = new @Promise
		promise.fulfill twitUserData
		promise
	).redirectPath '/'

everyauth.linkedin
	.consumerKey(settings.linkedin.appId)
	.consumerSecret(settings.linkedin.secret)
	.findOrCreateUser((sessions, token, secretToken, linkedinData) ->
		promise = new @Promise
		promise.fulfill linkedinData
		promise
	).redirectPath '/'

everyauth.google
	.appId(settings.google.appId)
	.appSecret(settings.google.secret)
	.callbackPath('/auth/google/callback')
	.scope('https://www.googleapis.com/auth/userinfo.profile https://www.google.com/m8/feeds/')
	.findOrCreateUser((sessions, token, secretToken, googleData) ->
		promise = new @Promise
		promise.fulfill googleData
		promise
	).redirectPath '/'


# make a mongoose connection
mongoose = require 'mongoose'
mongoose.connect settings.db
everyauth = require 'everyauth'

# get the global variables here
router = require './router'

MemStore = express.session.MemoryStore

app_root =  __dirname


# everyauth.debug = true
# console.log middleware
exports.startServer = (port, path, callback = (->)) ->
	# Starting the server here
	server = express.createServer()

	# declare middle ware here
	server.configure ->
		#server.use express.logger()
		server.use express.bodyParser()
		server.use express.methodOverride()
		server.use express.cookieParser()
		server.use express.session
			secret : 'adofi02394lk324j2lkjlsdfjlsdj09329jklsdajlfd'
			store : new MemStore
				reapInterval : 50000 * 10
		# express.favicon()
		# server.use express.csrf()
		server.use everyauth.middleware()
		server.use router
		
		server.set 'views', sysPath.join(app_root, 'templates')
		server.set('view engine', 'jade')
		server.set 'view options', layout : false

		server.use express.static path # path == public
		#everyauth.helpExpress(server)
		# server.use (req, response, next) ->
		# 	response.header 'Cache-Control', 'no-cache'
		# 	next()
		server.use express.errorHandler
					dumpExceptions: true
					showStack: true
	# server.register '.html',
	# 	compile : (str, options) ->
 #    		(locals) ->
 #      			str

	server.get '/', router.index

	# ===================
	# Jobs
	# ===================
	server.post '/api/v1/jobs', middleware.authorized, router.job.resource.post
	# Get request
	# server.get '/api/v1/jobs', middleware.authorized, router.job.resource.get
	server.get '/api/v1/jobs', router.job.resource.get
	server.get '/api/v1/jobs/:id', middleware.authorized, router.job.resource.get
	server.post '/api/v1/jobs/:id/:action', middleware.authorized, router.job.resource.action
	# put request
	server.put '/api/v1/jobs/:id', middleware.authorized, router.job.resource.put

	server.get '/jobs/add', middleware.authorized, router.job.pages.add
	server.get '/jobs/:id', middleware.authorized, router.job.pages.detail

	server.get '/jobs', middleware.authorized, router.job.pages.index

	# Publish
	# server.get '/api/v1/publish', middleware.authorized, router.publish.resource.get
	# server.post '/api/v1/publish', middleware.authorized, router.publish.resource.post

	# everyauth helper
	everyauth.helpExpress(server)

	server.listen parseInt port, 10
	server.on 'listening', callback
	server