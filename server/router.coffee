sysPath = require 'path'
path = './public'
models = require './models'

job = require './job'
publish = require './publish'

test = require './test'


index = (req, res) ->
	console.log "[App] Rendering '/' uri"
	if req.loggedIn
		# console.log req.session.auth.twitter
		console.log req.session.auth
		context =
			title : 'My application'
			session : req.session.auth.twitter
		res.render "app", context
	else
		context =
			title : 'My Title'
		res.render 'index', context

login = (req, res) ->
	console.log "[App] Rendering '/login' uri"
	#res.sendfile sysPath.join path, 'login.html'
	res.render 'login'

# jobs = (req, res, next) ->
#   console.log "[App] Fetching resuls from jobs database"
#   query = models.JobModel.find {}
#   query.exec (err, docs) ->
#     res.json docs

# seekers = (req, res, next) ->
#   console.log "[App] Fetching resuls from jobs database"
#   query = models.SeekerModel.find {}
#   query.exec (err, docs) ->
#     res.json docs

exports.index = index
exports.login = login
exports.job = job
exports.publish = publish

exports.test = test
# exports.jobs = jobs
# exports.seekers = seekers