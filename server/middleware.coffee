loadUser = (req, res, next) ->
	# do somethign
	console.log "Running loadUser MiddleWare"
	if req.session.user_id
		req.redirect "/me"
	else
		next()

authorized = (req, res, next) ->
	# if authorzied then do whatever you want
	if req.loggedIn
		next()
	# else the error
	else 
		error =
			error : "Error you are not authorized to do this"
		res.json error

module.exports = 
	loadUser : loadUser
	authorized : authorized