models = require './models'

detail = (req, res) ->
	msg = null
	unless req.params.id
		msg =
			error : "Id not specified"
		return res.json msg

	id = req.params.id
	query = models.PublishModel.findById id
	query.exec (err, doc) ->
		res.render 'publish/detail', doc


index = (req, res) ->
	query = models.PublishModel.find {}
	msg = null
	query.exec (err, docs) ->
		if err
			msg = err
		params =
			jobs : docs
		res.render 'publish/list', params

add = (req, res) ->
	res.render 'publish/add'

exports.index = index
exports.detail = detail
exports.add = add