# sysPath = require 'path'
# path = './public'
# models = require './models'

# post = (req, res) ->
# 	console.log "[App] Post 'api/v1/publish' "
# 	next = req.body['next']
# 	# options
# 	name = req.body['name']
# 	description = req.body['description']
# 	# initialize model
# 	model = new models.PublishModel
# 	model.name = name
# 	model.description = description
# 	msg = null
# 	model.save (err) ->
# 		if err
# 			msg = err

# 	# if we have msg then 
# 	if msg
# 		return res.json msg
# 	# if we have next then redirect
# 	if next
# 		res.redirect next
# 	else
# 		res.send model

sysPath = require 'path'
path = './public'
models = require './models'

Resources = require '../lib/resources'

class Publish extends Resources
	model : models.PublishModel
	name : 'publish'

	initialize : (params) ->
		@on 'iimjob', @iimjob

	iimjob : () ->
		console.log 'iimjobs'

	post : (req, res) ->
		console.log req
		@emit 'iimjob'
		console.log "[*] super"
		super

x = new Publish()

exports.post = x.post
exports.put = x.put
exports.get = x.get

# post = (req, res) ->
# 	msg =
# 		a : '10'
# 	console.log req.body
	
# 	res.json msg

# get = (req, res) ->
# 	query = null
# 	# check params
# 	if req.params.id
# 		id = req.params.id
# 		console.log "[App] Get 'api/v1/publish/#{id}' "
# 		# Query if params
# 		query = models.PublishModel.findById id
# 	else
# 		console.log "[App] Get 'api/v1/publish' "
# 		# query if no params
# 		query = models.PublishModel.find {}
# 	# query = models.PublishModel.find {}
# 	query.exec (err, docs) ->
# 		if err
# 			res.json err
# 		else
# 			res.json docs

# put = (req, res) ->
# 	query = null

# 	unless req.params.id
# 		msg =
# 			error : "Id not specified"
# 		res.json msg
# 	id = req.params.id
# 	query = models.PublishModel.findById id
# 	query.exec (err, doc) ->
# 		if err
# 			res.json err
# 		else
# 			doc.name = req.body['name']
# 			doc.description = req.body['description']
# 			doc.save()
# 			res.json doc

# exports.post = post

# exports.put = put
# exports.get = get