EventEmitter = require('events').EventEmitter
_ = require 'underscore'

class Resources extends EventEmitter

	name : ''

	model : null

	constructor : ->
		@initialize arguments...

	initialize : (options={}) ->
		_.extend @, options

		console.log "initializing Resources"

	get : (req, res) =>
		# console.log arguments
		query = null
		# check params
		if req.params.id
			id = req.params.id
			console.log "[App] Get 'api/v1/#{@name}/#{id}' "
			# Query if params
			query = @model.findById id
		else
			console.log "[App] Get 'api/v1/#{@name}' "
			# query if no params
			query = @model.find {}
		# query = models.JobModel.find {}
		query.exec (err, docs) ->
			if err
				res.json err
			else
				res.json docs

	post: (req, res) =>
		console.log "[App] Post 'api/v1/#{@name}' "
		next = req.body['next']
		# options
		name = req.body['name']
		description = req.body['description']
		# initialize model
		model = new @model
		model.name = name
		model.description = description
		msg = null
		model.save (err) ->
			if err
				msg = err

		# if we have msg then 
		if msg
			return res.json msg
		# if we have next then redirect
		if next
			res.redirect next
		else
			res.send model

	put : (req, res) =>
		query = null

		unless req.params.id
			msg =
				error : "Id not specified"
			res.json msg
		id = req.params.id
		query = @model.findById id
		query.exec (err, doc) ->
			if err
				res.json err
			else
				doc.name = req.body['name']
				doc.description = req.body['description']
				doc.save()
				res.json doc

	del : (req, res) =>
		res.send "Delete function #{constructor.name}"

	action : (req, res) =>
		action = req.params.action
		# console.log action
		# console.log "Action function"
		if @[action]?
			@[action](req, res)
		else
			res.send "Yahan par koi error #{constructor.name}"

module.exports = Resources