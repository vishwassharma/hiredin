_ = require 'underscore'
$ = require 'jquery'
Browser = require 'zombie'
events = require 'events'

DS = require('../ds')

# IIMJOBs ka automation
class Browzy

	_.extend @::, new events.EventEmitter

	# browser will the instance of zombie
	browser : null

	rank : 0

	steps : {}

	toVisit : {}

	constructor : () ->
		# initialize the browser
		@browser = new Browser()
		@rank = 0
		@browser.site = "http://dev.hirevoice.com:3000"
		# a function that can be overridden
		@steps = new DS.LinkedList()
		@initialize arguments...

	initialize : (options) ->
		@on 'pageloaded', @pageloaded

	visit : (url, callback) ->
		@browser.visit(url, debug: false, callback)

	pageloaded : (attr...) =>
		console.log attr

	endLife : () ->
		console.log "LifeEnds"

	# steps.add
	# 		url : "http://dev.hirevoice.com:3333/jobs/add"
	# 		name : "login"
	# 		attr : attributes
	addStep : (params) ->
		@steps.add params

	mechanics : (list) ->
		@browser.visit list.data['url'], =>
				name = list.data['name']
				attr = list.data['attr']
				@[name](attr)
				# next				
				list = list.next
				if (list)
					@mechanics(list)
				else
					@endLife()

module.exports = Browzy