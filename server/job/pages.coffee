models = require './models'

detail = (req, res) ->
	msg = null
	unless req.params.id
		msg =
			error : "Id not specified"
		return res.json msg

	id = req.params.id
	query = models.JobModel.findById id
	query.exec (err, doc) ->
		res.render 'jobs/detail', doc


index = (req, res) ->
	query = models.JobModel.find {}
	msg = null
	query.exec (err, docs) ->
		if err
			msg = err
		params =
			jobs : docs
		res.render 'jobs/list', params

add = (req, res) ->
	res.render 'jobs/add'

exports.index = index
exports.detail = detail
exports.add = add