Model = require './models'
Resource = require './resource'
Pages = require './pages'

exports.resource = Resource
exports.model = Model
exports.pages = Pages