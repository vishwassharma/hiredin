DS = require('../lib/AU').DataStructure
Browzy = require('../lib/AU').Browzy

class IIMJobs extends Browzy

	visit_login : (attr) =>
		console.log "Visit login"
		# console.log @browser.html()

	login : (attr) =>
		console.log "login"
		# fill the form
		@browser.
			fill("name", attr["name"]).
			fill("description", attr["description"]).
			pressButton "save", =>
				console.log "done login"

	logout : (attr) =>
		console.log "logout"

	endLife : () ->
		process.exit(1)

module.exports = IIMJobs