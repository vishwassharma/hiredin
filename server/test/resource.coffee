sysPath = require 'path'
path = './public'
models = require './models'

Resources = require '../lib/resources'

class Test extends Resources
	model : models.TestModel
	name : 'test'

x = new Test()

exports.post = x.post
exports.put = x.put
exports.get = x.get