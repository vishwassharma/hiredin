(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return require(absolute);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    definition(module.exports, localRequire(name), module);
    var exports = cache[name] = module.exports;
    return exports;
  };

  var require = function(name) {
    var path = expand(name, '.');

    if (has(cache, path)) return cache[path];
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex];
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '"');
  };

  var define = function(bundle) {
    for (var key in bundle) {
      if (has(bundle, key)) {
        modules[key] = bundle[key];
      }
    }
  }

  globals.require = require;
  globals.require.define = define;
  globals.require.brunch = true;
})();

window.require.define({"App": function(exports, require, module) {
  (function() {
    var App, AppController, Application, BreadCrumbController, FooterController, HeaderController, Router;

    App = require('lib/application');

    Router = require('router');

    HeaderController = require('controllers/header_controller');

    BreadCrumbController = require('controllers/breadcrumb_controller');

    AppController = require('controllers/app_controller');

    FooterController = require('controllers/footer_controller');

    Application = (function() {

      _.extend(Application.prototype, App);

      Application.prototype.debug = false;

      function Application() {
        this.initialize.apply(this, arguments);
      }

      Application.prototype.initialize = function(options) {
        if (_.has(options, "debug")) this.debug = options.debug;
        this.initMediator();
        this.initController();
        this.initRouter();
        return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
      };

      Application.prototype.initRouter = function(options) {
        return this.router = new Router();
      };

      Application.prototype.initLayout = function(options) {
        return this.layout = new Layout();
      };

      Application.prototype.initMediator = function(options) {};

      Application.prototype.initController = function(options) {
        new HeaderController();
        new BreadCrumbController();
        new AppController();
        return new FooterController();
      };

      return Application;

    })();

    module.exports = Application;

  }).call(this);
  
}});

window.require.define({"controllers/app_controller": function(exports, require, module) {
  (function() {
    var AppController, AppView, Controller,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Controller = require('lib/controllers/controller');

    AppView = require("views/app_view");

    AppController = (function(_super) {

      __extends(AppController, _super);

      function AppController() {
        AppController.__super__.constructor.apply(this, arguments);
      }

      AppController.prototype.initialize = function(options) {
        AppController.__super__.initialize.apply(this, arguments);
        return this.view = new AppView;
      };

      return AppController;

    })(Controller);

    module.exports = AppController;

  }).call(this);
  
}});

window.require.define({"controllers/breadcrumb_controller": function(exports, require, module) {
  (function() {
    var BreadCrumbController, BreadCrumbView, Controller,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Controller = require('lib/controllers/controller');

    BreadCrumbView = require("views/breadcrumb_view");

    BreadCrumbController = (function(_super) {

      __extends(BreadCrumbController, _super);

      function BreadCrumbController() {
        BreadCrumbController.__super__.constructor.apply(this, arguments);
      }

      BreadCrumbController.prototype.initialize = function(options) {
        BreadCrumbController.__super__.initialize.apply(this, arguments);
        return this.view = new BreadCrumbView;
      };

      return BreadCrumbController;

    })(Controller);

    module.exports = BreadCrumbController;

  }).call(this);
  
}});

window.require.define({"controllers/footer_controller": function(exports, require, module) {
  (function() {
    var Controller, FooterController, FooterView,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Controller = require('lib/controllers/controller');

    FooterView = require("views/footer_view");

    FooterController = (function(_super) {

      __extends(FooterController, _super);

      function FooterController() {
        FooterController.__super__.constructor.apply(this, arguments);
      }

      FooterController.prototype.initialize = function(options) {
        FooterController.__super__.initialize.apply(this, arguments);
        return this.view = new FooterView;
      };

      return FooterController;

    })(Controller);

    module.exports = FooterController;

  }).call(this);
  
}});

window.require.define({"controllers/header_controller": function(exports, require, module) {
  (function() {
    var Controller, HeaderController, HeaderView,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Controller = require('lib/controllers/controller');

    HeaderView = require("views/header_view");

    HeaderController = (function(_super) {

      __extends(HeaderController, _super);

      function HeaderController() {
        HeaderController.__super__.constructor.apply(this, arguments);
      }

      HeaderController.prototype.initialize = function(options) {
        HeaderController.__super__.initialize.apply(this, arguments);
        return this.view = new HeaderView;
      };

      return HeaderController;

    })(Controller);

    module.exports = HeaderController;

  }).call(this);
  
}});

window.require.define({"controllers/home_page_controller": function(exports, require, module) {
  (function() {
    var Controller, HeaderController, HomePageView,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Controller = require('lib/controllers/controller');

    HomePageView = require("views/home_page_view");

    HeaderController = (function(_super) {

      __extends(HeaderController, _super);

      function HeaderController() {
        HeaderController.__super__.constructor.apply(this, arguments);
      }

      HeaderController.prototype.view = '';

      HeaderController.prototype.initialize = function(options) {
        HeaderController.__super__.initialize.apply(this, arguments);
        return this.subscribeEvent('home', this.main);
      };

      HeaderController.prototype.main = function(options) {
        return this.view = new HomePageView;
      };

      return HeaderController;

    })(Controller);

    module.exports = HeaderController;

  }).call(this);
  
}});

window.require.define({"initialize": function(exports, require, module) {
  (function() {
    var App;

    App = require('App');

    $(function() {
      var app;
      app = new App({
        debug: true
      });
      return Backbone.history.start();
    });

  }).call(this);
  
}});

window.require.define({"lib/application": function(exports, require, module) {
  (function() {
    var Application;

    Application = {
      layout: '',
      controllers: [],
      controllerByName: {},
      initRouter: function() {
        return console.log("init router");
      },
      initLayout: function() {
        return console.log("Init Layout");
      },
      initController: function() {
        return console.log("init controller");
      },
      initMediator: function() {
        return console.log("init Mediator");
      },
      addController: function(name, controller) {
        return true;
      },
      getControllerByName: function(name) {}
    };

    module.exports = Application;

  }).call(this);
  
}});

window.require.define({"lib/controllers/controller": function(exports, require, module) {
  (function() {
    var Controller, Subscriber, mediator,
      __hasProp = Object.prototype.hasOwnProperty;

    mediator = require('lib/mediator');

    Subscriber = require('lib/subscriber');

    Controller = (function() {

      _.extend(Controller.prototype, Subscriber);

      Controller.prototype.currentId = null;

      function Controller() {
        this.initialize.apply(this, arguments);
      }

      Controller.prototype.initialize = function(options) {};

      Controller.prototype.disposed = false;

      Controller.prototype.dispose = function() {
        var obj, prop, properties, _i, _len;
        if (this.disposed) return;
        for (prop in this) {
          if (!__hasProp.call(this, prop)) continue;
          obj = this[prop];
          if (obj && typeof obj.dispose === 'function') {
            obj.dispose();
            delete this[prop];
          }
        }
        this.unsubscribeAllEvents();
        properties = ['currentId'];
        for (_i = 0, _len = properties.length; _i < _len; _i++) {
          prop = properties[_i];
          delete this[prop];
        }
        this.disposed = true;
        return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
      };

      return Controller;

    })();

    module.exports = Controller;

  }).call(this);
  
}});

window.require.define({"lib/mediator": function(exports, require, module) {
  (function() {
    var mediator;

    mediator = {};

    mediator.channels = {};

    mediator.subscribe = mediator.on = Backbone.Events.on;

    mediator.publish = mediator.trigger = Backbone.Events.trigger;

    mediator.unsubscribe = mediator.off = Backbone.Events.off;

    module.exports = mediator;

  }).call(this);
  
}});

window.require.define({"lib/mixins/datastructure/linked_list": function(exports, require, module) {
  (function() {
    var LinkedList;

    module.exports = LinkedList = {
      get: function(options) {}
    };

  }).call(this);
  
}});

window.require.define({"lib/mixins/form": function(exports, require, module) {
  (function() {
    var Form;

    Form = {
      formId: "",
      updateOn: '',
      params: function() {
        console.log("Here");
        return $(this.formId).serializeArray();
      },
      data: '',
      bindCollection: function(collection) {
        return this.data = collection;
      },
      bindModel: function(model) {
        return this.data = model;
      },
      validate: function(event) {
        var data, result;
        event.preventDefault();
        data = this.formId.serializeArray();
        result = {};
        _.each(data, function(d) {
          return result[d['name']] = d['value'];
        });
        return this.data.create(result);
      },
      onSubmit: function(event) {
        console.log("On submit");
        event.preventDefault();
        debugger;
      },
      resetForm: function() {
        return console.log("Resetting form");
      },
      update: function(view) {
        var collection;
        if (!view) return;
        collection = view.collection;
        return collection.trigger('reset');
      }
    };

    module.exports = Form;

  }).call(this);
  
}});

window.require.define({"lib/mixins/url": function(exports, require, module) {
  (function() {
    var Url;

    module.exports = Url = {
      get: function(options) {}
    };

  }).call(this);
  
}});

window.require.define({"lib/mixins/utils": function(exports, require, module) {
  (function() {
    var Utils;

    module.exports = Utils = {
      fun: function(options) {}
    };

  }).call(this);
  
}});

window.require.define({"lib/models/collection": function(exports, require, module) {
  (function() {
    var Collection, Subscriber,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Subscriber = require('lib/subscriber');

    module.exports = Collection = (function(_super) {

      __extends(Collection, _super);

      function Collection() {
        Collection.__super__.constructor.apply(this, arguments);
      }

      _.extend(Collection.prototype, Subscriber);

      return Collection;

    })(Backbone.Collection);

  }).call(this);
  
}});

window.require.define({"lib/models/model": function(exports, require, module) {
  (function() {
    var Model, Subscriber,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; },
      __indexOf = Array.prototype.indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

    Subscriber = require('lib/subscriber');

    Model = (function(_super) {
      var serializeAttributes;

      __extends(Model, _super);

      function Model() {
        Model.__super__.constructor.apply(this, arguments);
      }

      _.extend(Model.prototype, Subscriber);

      Model.prototype.idAttribute = '_id';

      Model.prototype.getAttributes = function() {
        return this.attributes;
      };

      serializeAttributes = function(model, attributes, modelStack) {
        var delegator, item, key, value, _results;
        if (!modelStack) {
          console.log(attributes);
          delegator = utils.beget(attributes);
          modelStack = [model];
        } else {
          modelStack.push(model);
        }
        _results = [];
        for (key in attributes) {
          value = attributes[key];
          if (value instanceof Model) {
            if (delegator == null) delegator = utils.beget(attributes);
            _results.push(delegator[key] = value === model || __indexOf.call(modelStack, value) >= 0 ? null : serializeAttributes(value, value.getAttributes(), modelStack));
          } else if (value instanceof Backbone.Collection) {
            if (delegator == null) delegator = utils.beget(attributes);
            _results.push(delegator[key] = (function() {
              var _i, _len, _ref, _results2;
              _ref = value.models;
              _results2 = [];
              for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                item = _ref[_i];
                _results2.push(serializeAttributes(item, item.getAttributes(), modelStack));
              }
              return _results2;
            })());
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };

      Model.prototype.serialize = function(model) {
        return this.getAttributes();
      };

      return Model;

    })(Backbone.Model);

    module.exports = Model;

  }).call(this);
  
}});

window.require.define({"lib/router": function(exports, require, module) {
  (function() {
    var Router,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Router = (function(_super) {

      __extends(Router, _super);

      function Router() {
        Router.__super__.constructor.apply(this, arguments);
      }

      Router.prototype.initialize = function(options) {};

      return Router;

    })(Backbone.Router);

    module.exports = Router;

  }).call(this);
  
}});

window.require.define({"lib/services/service_provider": function(exports, require, module) {
  (function() {
    var ServiceProvider;

    ServiceProvider = (function() {

      function ServiceProvider() {
        this.initialize.apply(this, arguments);
      }

      ServiceProvider.initialize = function(options) {};

      ServiceProvider.prototype.load = function() {};

      ServiceProvider.prototype.refreshToken = function() {};

      ServiceProvider.prototype.validate_token = function() {};

      return ServiceProvider;

    })();

    module.exports = ServiceProvider;

  }).call(this);
  
}});

window.require.define({"lib/subscriber": function(exports, require, module) {
  (function() {
    var Subscriber, mediator;

    mediator = require('lib/mediator');

    Subscriber = {
      subscribeEvent: function(type, handler) {
        if (typeof type !== 'string') {
          throw new TypeError("1st argument should be a string");
        }
        if (typeof handler !== 'function') {
          throw new TypeError("2st argument should be a callback function");
        }
        mediator.unsubscribe(type, handler, this);
        return mediator.subscribe(type, handler, this);
      },
      unsubscribeEvent: function(type, handler) {
        if (typeof type !== 'string') {
          throw new TypeError("1st argument should be a string");
        }
        if (typeof handler !== 'function') {
          throw new TypeError("2st argument should be a callback function");
        }
        return mediator.unsubscribe(type, handler, this);
      },
      unsubscribeAllEvents: function() {
        return mediator.unsubscribe(null, null, this);
      }
    };

    if (typeof Object.freeze === "function") Object.freeze(Subscriber);

    module.exports = Subscriber;

  }).call(this);
  
}});

window.require.define({"lib/view_helper": function(exports, require, module) {
  (function() {



  }).call(this);
  
}});

window.require.define({"lib/views/collection_view": function(exports, require, module) {
  (function() {
    var CollectionView, View,
      __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    View = require('lib/views/view');

    module.exports = CollectionView = (function(_super) {

      __extends(CollectionView, _super);

      function CollectionView() {
        this.renderAllItems = __bind(this.renderAllItems, this);
        this.itemsResetted = __bind(this.itemsResetted, this);
        this.itemRemoved = __bind(this.itemRemoved, this);
        this.itemAdded = __bind(this.itemAdded, this);
        CollectionView.__super__.constructor.apply(this, arguments);
      }

      CollectionView.prototype.viewsByCid = null;

      CollectionView.prototype.visibleItems = null;

      CollectionView.prototype.listSelector = null;

      CollectionView.prototype.$list = null;

      CollectionView.prototype.initialize = function(options) {
        if (options == null) options = {};
        CollectionView.__super__.initialize.apply(this, arguments);
        _(options).defaults({
          render: true,
          renderItems: true,
          filterer: null
        });
        if (options.itemView != null) this.itemView = options.itemView;
        this.viewsByCid = {};
        this.visibleItems = [];
        if (this.model || this.collection) this.addCollectionListeners();
        if (options.render) this.render();
        if (options.renderItems && (this.model || this.collection)) {
          return this.renderAllItems();
        }
      };

      CollectionView.prototype.getView = function(model) {
        if (this.itemView != null) {
          return new this.itemView({
            model: model
          });
        } else {
          throw new Error('The CollectionView#itemView property must be\
  defined (or the getView() must be overridden)');
        }
      };

      CollectionView.prototype.addCollectionListeners = function() {
        this.modelBind('add', this.itemAdded);
        this.modelBind('remove', this.itemRemoved);
        return this.modelBind('reset', this.itemsResetted);
      };

      CollectionView.prototype.render = function() {
        CollectionView.__super__.render.apply(this, arguments);
        return this.$list = this.listSelector ? this.$(this.listSelector) : this.$el;
      };

      CollectionView.prototype.itemAdded = function(item, options) {
        if (options == null) options = {};
        console.log(item);
        console.log("item added");
        return this.renderAndInsertItem(item, options.index);
      };

      CollectionView.prototype.itemRemoved = function(item) {
        return console.log("Remove item");
      };

      CollectionView.prototype.itemsResetted = function() {
        console.log("Render all items " + this.constructor.name);
        return this.renderAllItems();
      };

      CollectionView.prototype.renderAllItems = function() {
        var cid, index, item, items, remainingViewsByCid, view, _i, _len, _len2, _ref;
        items = this.collection.models;
        this.visibleItems = [];
        remainingViewsByCid = {};
        for (_i = 0, _len = items.length; _i < _len; _i++) {
          item = items[_i];
          view = this.viewsByCid[item.cid];
          if (view) remainingViewsByCid[item.cid] = view;
        }
        _ref = this.viewsByCid;
        for (cid in _ref) {
          if (!__hasProp.call(_ref, cid)) continue;
          view = _ref[cid];
          if (!(cid in remainingViewsByCid)) this.removeView(cid, view);
        }
        for (index = 0, _len2 = items.length; index < _len2; index++) {
          item = items[index];
          view = this.viewsByCid[item.cid];
          if (view) {
            this.insertView(item, view, index, 0);
          } else {
            this.renderAndInsertItem(item, index);
          }
        }
        if (!items.length) {
          return this.trigger('visibilityChange', this.visibleItems);
        }
      };

      CollectionView.prototype.renderAndInsertItem = function(item, index) {
        var view;
        view = this.renderItem(item);
        return this.insertView(item, view, index);
      };

      CollectionView.prototype.renderItem = function(item) {
        var view;
        view = this.viewsByCid[item.cid];
        if (!view) {
          view = this.getView(item);
          this.viewsByCid[item.cid] = view;
        }
        view.render();
        return view;
      };

      CollectionView.prototype.insertView = function(item, view, index, animationDuration) {
        var $list, $next, $previous, $viewEl, children, length, position, viewEl;
        if (index == null) index = null;
        if (animationDuration == null) animationDuration = this.animationDuration;
        position = typeof index === 'number' ? index : this.collection.indexOf(item);
        viewEl = view.el;
        $viewEl = view.$el;
        $list = this.$list;
        children = $list.children(this.itemSelector || void 0);
        length = children.length;
        if (length === 0 || position === length) {
          $list.append(viewEl);
        } else {
          if (position === 0) {
            $next = children.eq(position);
            $next.before(viewEl);
          } else {
            $previous = children.eq(position - 1);
            $previous.after(viewEl);
          }
        }
        return view.trigger('addedToDOM');
      };

      CollectionView.prototype.removeView = function(cid, view) {
        view.dispose();
        return delete this.viewsByCid[cid];
      };

      CollectionView.prototype.dispose = function() {
        var cid, prop, properties, view, _i, _len, _ref;
        if (this.disposed) return;
        _ref = this.viewsByCid;
        for (cid in _ref) {
          if (!__hasProp.call(_ref, cid)) continue;
          view = _ref[cid];
          view.dispose();
        }
        properties = ['$list', '$fallback', '$loading', 'viewsByCid', 'visibleItems'];
        for (_i = 0, _len = properties.length; _i < _len; _i++) {
          prop = properties[_i];
          delete this[prop];
        }
        return CollectionView.__super__.dispose.apply(this, arguments);
      };

      return CollectionView;

    })(View);

  }).call(this);
  
}});

window.require.define({"lib/views/layout": function(exports, require, module) {
  (function() {
    var Layout, Subscriber, mediator;

    mediator = require('lib/mediator');

    Subscriber = require('lib/subscriber');

    Layout = (function() {

      _.extend(Layout.prototype, Subscriber);

      Layout.prototype.title = '';

      Layout.prototype.el = document;

      Layout.prototype.$el = $(document);

      function Layout() {
        this.initialize.apply(this, arguments);
      }

      Layout.prototype.initialize = function(options) {
        return this.title = options.title;
      };

      Layout.prototype.disposed = false;

      Layout.prototype.dispose = function() {
        if (this.disposed) return;
        this.unsubscribeAllEvents();
        delete this.title;
        this.disposed = true;
        return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
      };

      return Layout;

    })();

    module.exports = Layout;

  }).call(this);
  
}});

window.require.define({"lib/views/view": function(exports, require, module) {
  (function() {
    var Subscriber, View,
      __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    require('lib/view_helper');

    Subscriber = require('lib/subscriber');

    module.exports = View = (function(_super) {

      __extends(View, _super);

      function View() {
        this.render = __bind(this.render, this);
        this.addSubView = __bind(this.addSubView, this);
        View.__super__.constructor.apply(this, arguments);
      }

      _(View.prototype).extend(Subscriber);

      View.prototype.autoRender = false;

      View.prototype.hasSubView = false;

      View.prototype.subViews = null;

      View.prototype.subViewsByName = null;

      View.prototype.filter = false;

      View.prototype.container = null;

      View.prototype.containerMethod = 'append';

      View.prototype.initialize = function(options) {
        if (options == null) options = {};
        _.extend(this, options);
        if (this.hasSubView) {
          this.subViews = [];
          this.subViewsByName = {};
        }
        if (this.autoRender) return this.render();
      };

      View.prototype.addSubView = function(name, view, attr) {
        var options, v;
        if (attr) {
          options = _.extend({}, options, attr);
        } else {
          new Error("Should have attributes here");
        }
        v = new view(options);
        v.parent = this;
        this.subViews.push(v);
        this.subViewsByName[name] = v;
        return v;
      };

      View.prototype.template = function() {};

      View.prototype.getTemplateData = function() {
        var items, model, templateData, _i, _len, _ref;
        if (this.model) {
          templateData = this.model.serialize();
        } else if (this.collection) {
          items = [];
          _ref = this.collection.models;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            model = _ref[_i];
            items.push(model.serialize());
          }
          templateData = {
            items: items
          };
        } else {
          templateData = {};
        }
        return templateData;
      };

      View.prototype.getRenderData = function() {};

      View.prototype.render = function(attr) {
        var data, html;
        if (attr == null) attr = null;
        this.beforeRender();
        console.debug("Rendering " + this.constructor.name);
        data = this.getTemplateData();
        if (attr) data = _.extend({}, data, attr);
        if (this.filter) data = this.filter(data);
        html = this.template(data);
        this.$el.html(html);
        this.afterRender();
        return this;
      };

      View.prototype.beforeRender = function() {};

      View.prototype.afterRender = function() {
        if (this.container) {
          $(this.container)[this.containerMethod](this.el);
          this.trigger('addedToDom');
        }
        return this;
      };

      View.prototype.delegate = function(eventType, second, third) {
        var handler, selector;
        if (typeof eventType !== 'string') {
          throw new TypeError('View#delegate: first argument must be a string');
        }
        if (arguments.length === 2) {
          handler = second;
        } else if (arguments.length === 3) {
          selector = second;
          if (typeof selector !== 'string') {
            throw new TypeError('View#delegate: ' + 'second argument must be a string');
          }
          handler = third;
        } else {
          throw new TypeError('View#delegate: ' + 'only two or three arguments are allowed');
        }
        if (typeof handler !== 'function') {
          throw new TypeError('View#delegate: ' + 'handler argument must be function');
        }
        eventType += ".delegate" + this.cid;
        handler = _(handler).bind(this);
        if (selector) {
          this.$el.on(eventType, selector, handler);
        } else {
          this.$el.on(eventType, handler);
        }
        return handler;
      };

      View.prototype.undelegate = function() {
        return this.$el.unbind(".delegate" + this.cid);
      };

      View.prototype.modelBind = function(type, handler) {
        var modelOrCollection;
        if (typeof type !== 'string') {
          throw new TypeError('View#modelBind: ' + 'type must be a string');
        }
        if (typeof handler !== 'function') {
          throw new TypeError('View#modelBind: ' + 'handler argument must be function');
        }
        modelOrCollection = this.model || this.collection;
        if (!modelOrCollection) {
          throw new TypeError('View#modelBind: no model or collection set');
        }
        modelOrCollection.off(type, handler, this);
        return modelOrCollection.on(type, handler, this);
      };

      View.prototype.modelUnbind = function(type, handler) {
        var modelOrCollection;
        if (typeof type !== 'string') {
          throw new TypeError('View#modelUnbind: ' + 'type argument must be a string');
        }
        if (typeof handler !== 'function') {
          throw new TypeError('View#modelUnbind: ' + 'handler argument must be a function');
        }
        modelOrCollection = this.model || this.collection;
        if (!modelOrCollection) return;
        return modelOrCollection.off(type, handler);
      };

      View.prototype.modelUnbindAll = function() {
        var modelOrCollection;
        modelOrCollection = this.model || this.collection;
        if (!modelOrCollection) return;
        return modelOrCollection.off(null, null, this);
      };

      View.prototype.disposed = false;

      View.prototype.dispose = function() {
        var prop, properties, _i, _len;
        if (this.disposed) return;
        this.unsubscribeAllEvents();
        this.modelUnbindAll();
        this.off();
        this.$el.remove();
        properties = ['el', '$el', 'options', 'model', 'collection', 'subviews', 'subviewsByName', '_callbacks'];
        for (_i = 0, _len = properties.length; _i < _len; _i++) {
          prop = properties[_i];
          delete this[prop];
        }
        this.disposed = true;
        return typeof Object.freeze === "function" ? Object.freeze(this) : void 0;
      };

      return View;

    })(Backbone.View);

  }).call(this);
  
}});

window.require.define({"models/job": function(exports, require, module) {
  (function() {
    var Job, Model,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Model = require("lib/models/model");

    module.exports = Job = (function(_super) {

      __extends(Job, _super);

      function Job() {
        Job.__super__.constructor.apply(this, arguments);
      }

      return Job;

    })(Model);

  }).call(this);
  
}});

window.require.define({"router": function(exports, require, module) {
  (function() {
    var AppRouter, HomePageController, Router, mediator,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    Router = require('lib/router');

    HomePageController = require('controllers/home_page_controller');

    mediator = require('lib/mediator');

    module.exports = AppRouter = (function(_super) {

      __extends(AppRouter, _super);

      function AppRouter() {
        AppRouter.__super__.constructor.apply(this, arguments);
      }

      AppRouter.prototype.initialize = function() {
        return this.homePage = new HomePageController;
      };

      AppRouter.prototype.routes = {
        '': 'home'
      };

      AppRouter.prototype.home = function() {
        console.log("HomePage");
        return mediator.publish('home');
      };

      return AppRouter;

    })(Router);

  }).call(this);
  
}});

window.require.define({"views/app_view": function(exports, require, module) {
  (function() {
    var AppView, View, template,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    View = require('lib/views/view');

    template = require('views/templates/app');

    module.exports = AppView = (function(_super) {

      __extends(AppView, _super);

      function AppView() {
        AppView.__super__.constructor.apply(this, arguments);
      }

      AppView.prototype.template = template;

      AppView.prototype.autoRender = true;

      AppView.prototype.container = "#main";

      AppView.prototype.id = "app";

      AppView.prototype.className = "row";

      return AppView;

    })(View);

  }).call(this);
  
}});

window.require.define({"views/breadcrumb_view": function(exports, require, module) {
  (function() {
    var BreadcrumbView, View, template,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    View = require('lib/views/view');

    template = require('views/templates/breadcrumb');

    module.exports = BreadcrumbView = (function(_super) {

      __extends(BreadcrumbView, _super);

      function BreadcrumbView() {
        BreadcrumbView.__super__.constructor.apply(this, arguments);
      }

      BreadcrumbView.prototype.template = template;

      BreadcrumbView.prototype.container = '#main';

      BreadcrumbView.prototype.id = "breadcrumb";

      BreadcrumbView.prototype.className = "row";

      BreadcrumbView.prototype.autoRender = true;

      return BreadcrumbView;

    })(View);

  }).call(this);
  
}});

window.require.define({"views/footer_view": function(exports, require, module) {
  (function() {
    var FooterView, View, template,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    View = require('lib/views/view');

    template = require('views/templates/footer');

    module.exports = FooterView = (function(_super) {

      __extends(FooterView, _super);

      function FooterView() {
        FooterView.__super__.constructor.apply(this, arguments);
      }

      FooterView.prototype.template = template;

      FooterView.prototype.container = "#main";

      FooterView.prototype.id = "footer";

      FooterView.prototype.className = "row";

      FooterView.prototype.autoRender = true;

      return FooterView;

    })(View);

  }).call(this);
  
}});

window.require.define({"views/header_view": function(exports, require, module) {
  (function() {
    var HeaderView, View, template,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    View = require('lib/views/view');

    template = require('views/templates/header');

    HeaderView = (function(_super) {

      __extends(HeaderView, _super);

      function HeaderView() {
        HeaderView.__super__.constructor.apply(this, arguments);
      }

      HeaderView.prototype.template = template;

      HeaderView.prototype.autoRender = true;

      HeaderView.prototype.container = "#main";

      HeaderView.prototype.className = "navbar";

      HeaderView.prototype.id = "header";

      return HeaderView;

    })(View);

    module.exports = HeaderView;

  }).call(this);
  
}});

window.require.define({"views/home/job": function(exports, require, module) {
  (function() {
    var JobView, View, template,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    View = require('lib/views/view');

    template = require('views/templates/home/job');

    module.exports = JobView = (function(_super) {

      __extends(JobView, _super);

      function JobView() {
        JobView.__super__.constructor.apply(this, arguments);
      }

      JobView.prototype.template = template;

      JobView.prototype.tagName = 'li';

      JobView.prototype.className = 'row';

      return JobView;

    })(View);

  }).call(this);
  
}});

window.require.define({"views/home/jobs": function(exports, require, module) {
  (function() {
    var CollectionView, JobView, JobsView, template,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    CollectionView = require('lib/views/collection_view');

    template = require('views/templates/home/jobs');

    JobView = require('views/home/job');

    module.exports = JobsView = (function(_super) {

      __extends(JobsView, _super);

      function JobsView() {
        JobsView.__super__.constructor.apply(this, arguments);
      }

      JobsView.prototype.template = template;

      JobsView.prototype.tagName = 'div';

      JobsView.prototype.listSelector = '#jobs';

      JobsView.prototype.className = "span7";

      JobsView.prototype.itemView = JobView;

      return JobsView;

    })(CollectionView);

  }).call(this);
  
}});

window.require.define({"views/home_page_view": function(exports, require, module) {
  (function() {
    var Collection, Form, HomePageView, View, template,
      __hasProp = Object.prototype.hasOwnProperty,
      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor; child.__super__ = parent.prototype; return child; };

    View = require('lib/views/view');

    Collection = require('lib/models/collection');

    Form = require('lib/mixins/form');

    template = require('views/templates/home_page');

    module.exports = HomePageView = (function(_super) {

      __extends(HomePageView, _super);

      function HomePageView() {
        HomePageView.__super__.constructor.apply(this, arguments);
      }

      HomePageView.prototype.template = template;

      HomePageView.prototype.autoRender = true;

      HomePageView.prototype.container = '#app';

      HomePageView.prototype.className = 'span12';

      HomePageView.prototype.id = 'home-page';

      HomePageView.prototype.hasSubView = true;

      _.extend(HomePageView.prototype, Form);

      HomePageView.prototype.initialize = function(options) {
        HomePageView.__super__.initialize.apply(this, arguments);
        this.formId = this.$("#addJob");
        this.delegate('click', '#submit_btn', this.validate);
        this.delegate('submit', '#addJob', this.onSubmit);
        return this.addJobView();
      };

      HomePageView.prototype.addJobView = function() {
        var JobsView, collection, options;
        JobsView = require("views/home/jobs");
        collection = new Collection();
        collection.model = require('models/job');
        collection.url = "/jobs";
        this.bindCollection(collection);
        options = {
          collection: collection,
          container: '#jobsview'
        };
        this.jobsView = this.addSubView('jobsView', JobsView, options);
        return collection.fetch();
      };

      return HomePageView;

    })(View);

  }).call(this);
  
}});

window.require.define({"views/templates/app": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", foundHelper, self=this;


    return buffer;});
}});

window.require.define({"views/templates/breadcrumb": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<div class=\"span12\">\r\n	<h1>Breadcrumb</h1>\r\n</div>";});
}});

window.require.define({"views/templates/footer": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<h1 class=\"span12\">Footer</h1>";});
}});

window.require.define({"views/templates/header": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


    buffer += "<div class=\"navbar-inner\">\r\n	<div class=\"container\">\r\n		<a href=\"#\" class=\"brand\">hiredin</a>\r\n		<!-- left nav -->\r\n		<ul class=\"nav\">\r\n			<li><a href=\"#\">Home</a></li>\r\n			<li><a href=\"#\">Pricing</a></li>\r\n			<li><a href=\"#\">Tour</a></li>\r\n			<li><a href=\"#\">Learn</a></li>\r\n		</ul>\r\n		<!-- left nav ends -->\r\n		<ul class=\"nav pull-right\">\r\n			<li><a href=\"";
    foundHelper = helpers.everyauth;
    stack1 = foundHelper || depth0.everyauth;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.google);
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.user);
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.link);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "everyauth.google.user.link", { hash: {} }); }
    buffer += escapeExpression(stack1) + "\">";
    foundHelper = helpers.everyauth;
    stack1 = foundHelper || depth0.everyauth;
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.google);
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.user);
    stack1 = (stack1 === null || stack1 === undefined || stack1 === false ? stack1 : stack1.name);
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "everyauth.google.user.name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</a></li>\r\n			<li class=\"divider-vertical\"></li>\r\n			<li><a href=\"/logout\">Logout</a></li>\r\n			<!-- dropdown -->\r\n		</ul>\r\n		<!-- Right nav ends -->\r\n	</div>\r\n	<!-- Header container -->\r\n</div>\r\n<!-- nav bar inner -->\r\n";
    return buffer;});
}});

window.require.define({"views/templates/home/job": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var buffer = "", stack1, foundHelper, self=this, functionType="function", helperMissing=helpers.helperMissing, undef=void 0, escapeExpression=this.escapeExpression;


    buffer += "<div class=\"span12\">\r\n	<div class=\"row\">\r\n		<p class=\"title span5 HV-text-overflow\">";
    foundHelper = helpers.name;
    stack1 = foundHelper || depth0.name;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "name", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</p>\r\n		<p class=\"span7 HV-text-overflow\">";
    foundHelper = helpers.description;
    stack1 = foundHelper || depth0.description;
    if(typeof stack1 === functionType) { stack1 = stack1.call(depth0, { hash: {} }); }
    else if(stack1=== undef) { stack1 = helperMissing.call(depth0, "description", { hash: {} }); }
    buffer += escapeExpression(stack1) + "</p>\r\n	</div>\r\n</div>";
    return buffer;});
}});

window.require.define({"views/templates/home/jobs": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<h3 class=\"title\">Jobs</h3>\r\n<ul id=\"jobs\" class=\"unstyled row\"></ul>";});
}});

window.require.define({"views/templates/home_page": function(exports, require, module) {
  module.exports = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
    helpers = helpers || Handlebars.helpers;
    var foundHelper, self=this;


    return "<div class=\"row\">\r\n	<div class=\"span12\">\r\n		<div class=\"well\">\r\n			<h2>Add Job</h2>\r\n			<p>Lorem ipsum id aliquip ut nulla in Duis cillum minim. </p>\r\n			<!-- Model -->\r\n			<div class=\"modal hide\" id=\"myModal\">\r\n				<div class=\"modal-header\">\r\n					<button type=\"button\" class=\"close\" data-dismiss=\"modal\">×</button>\r\n					<h3>Add Job</h3>\r\n				</div>\r\n				<div class=\"modal-body\">\r\n					<form class=\"form-horizontal\" method=\"post\" action=\"/jobs/\" id=\"addJob\">\r\n						<fieldset>\r\n							<div class=\"control-group\">\r\n								<label class=\"control-label\" for=\"name\">Job name</label>\r\n								<div class=\"controls\">\r\n									<input type=\"text\" class=\"input-xlarge\" name=\"name\">					\r\n									<p class=\"help-block\">eg. Business Developer, PHP Developer</p>\r\n								</div>\r\n							</div>\r\n							<div class=\"control-group\">\r\n								<label class=\"control-label\" for=\"description\">Description</label>\r\n								<div class=\"controls\">\r\n									<textarea class=\"input-xlarge\" name=\"description\" rows=\"10\"></textarea>\r\n								</div>\r\n							</div>\r\n							<div class=\"form-actions\">\r\n								<button type=\"submit\" class=\"btn btn-primary\" id=\"submit_btn\" name=\"submit\">Save changes</button>\r\n								<button class=\"btn\">Cancel</button>\r\n							</div>\r\n						</fieldset>\r\n					</form>\r\n				</div>\r\n				<div class=\"modal-footer\">\r\n					<a href=\"#\" class=\"btn\" data-dismiss=\"modal\">Close</a>\r\n					<a href=\"#\" class=\"btn btn-primary\">Next</a>\r\n				</div>\r\n			</div>\r\n			<!-- End model -->\r\n			<a href=\"#myModal\" data-toggle=\"modal\" class=\"btn btn-primary btn-large\">Add New Job</a>\r\n		</div>\r\n	</div>\r\n</div>\r\n<div class=\"row\">\r\n	<div class=\"span12\">\r\n		<div class=\"row\">\r\n			<p class=\"span12\">Lorem ipsum Excepteur aute et veniam consectetur id esse. Lorem ipsum eu adipisicing ad est quis voluptate reprehenderit anim eiusmod incididunt voluptate fugiat officia ullamco minim. Lorem ipsum cillum mollit fugiat eu anim elit exercitation adipisicing reprehenderit in Ut labore reprehenderit. </p>\r\n		</div>	\r\n		<div id=\"jobsview\" class=\"row\"></div>\r\n	</div>\r\n</div>";});
}});

