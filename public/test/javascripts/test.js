(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return require(absolute);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    definition(module.exports, localRequire(name), module);
    var exports = cache[name] = module.exports;
    return exports;
  };

  var require = function(name) {
    var path = expand(name, '.');

    if (has(cache, path)) return cache[path];
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex];
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '"');
  };

  var define = function(bundle) {
    for (var key in bundle) {
      if (has(bundle, key)) {
        modules[key] = bundle[key];
      }
    }
  }

  globals.require = require;
  globals.require.define = define;
  globals.require.brunch = true;
})();

window.require.define({"test/controllers/app_controller_test": function(exports, require, module) {
  (function() {
    var AppController;

    AppController = require('controllers/app_controller');

    describe("[APP] AppController", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(AppController)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/controllers/breadcrum_controller_test": function(exports, require, module) {
  (function() {
    var BreadBrumbController;

    BreadBrumbController = require('controllers/breadcrumb_controller');

    describe("[APP] BreadBrumbController", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(BreadBrumbController)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/controllers/footer_controller_test": function(exports, require, module) {
  (function() {
    var FooterController;

    FooterController = require('controllers/footer_controller');

    describe("[APP] FooterController", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(FooterController)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/controllers/header_controller_test": function(exports, require, module) {
  (function() {
    var HeaderController;

    HeaderController = require('controllers/header_controller');

    describe("[APP] HeaderController", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(HeaderController)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/controllers/home_page_controller_test": function(exports, require, module) {
  (function() {
    var HomePageView;

    HomePageView = require('views/home_page_view');

    describe("[APP] HomePageView", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(HomePageView)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/application_test": function(exports, require, module) {
  (function() {
    var application;

    application = require('lib/application');

    describe("[Lib] Application Test", function() {
      it("should be a object", function() {
        return expect(_.isObject(application)).to.equal(true);
      });
      it("should have function initRouter", function() {
        return expect(application.initRouter).to.be.a('function');
      });
      it("should have function initLayout", function() {
        return expect(application.initLayout).to.be.a('function');
      });
      it("should have function initController", function() {
        return expect(application.initController).to.be.a('function');
      });
      it("should have function getControllerByName", function() {
        return expect(application.getControllerByName).to.be.a('function');
      });
      return describe("Controller Related Function", function() {
        it("should cache controllers", function() {
          expect(application.controllers).to.be.a('array');
          return expect(application.controllerByName).to.be.a('object');
        });
        describe("#addController function", function() {
          it("should fails when arguments are not given", function() {});
          return it("should take check arguments sanity", function() {});
        });
        return describe("#getControllerByName function", function() {
          it("should fails when arguments are not given", function() {});
          return it("should take check arguments sanity", function() {});
        });
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/collection_test": function(exports, require, module) {
  (function() {
    var Collection, Subscriber,
      __hasProp = Object.prototype.hasOwnProperty;

    Collection = require('lib/models/collection');

    Subscriber = require('lib/subscriber');

    describe("[LIB] Collection", function() {
      var collect;
      collect = null;
      beforeEach(function() {
        return collect = new Collection;
      });
      afterEach(function() {});
      it("should be exported object", function() {
        return expect(_.isObject(Collection)).to.equal(true);
      });
      return it("should mixing subscriber", function() {
        var name, value, _results;
        _results = [];
        for (name in Subscriber) {
          if (!__hasProp.call(Subscriber, name)) continue;
          value = Subscriber[name];
          _results.push(expect(collect[name]).to.equal(Subscriber[name]));
        }
        return _results;
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/collection_view_test": function(exports, require, module) {
  (function() {
    var CollectionView, Subscriber,
      __hasProp = Object.prototype.hasOwnProperty;

    CollectionView = require('lib/views/collection_view');

    Subscriber = require('lib/subscriber');

    describe("[LIB] CollectionView", function() {
      var view;
      view = null;
      beforeEach(function() {
        return view = new CollectionView;
      });
      afterEach(function() {
        return view.dispose();
      });
      it("should be defined", function() {
        return expect(_.isObject(CollectionView)).to.equal(true);
      });
      it("should have some default function", function() {
        expect(view.render).to.be.a('function');
        expect(view.addSubView).to.be.a('function');
        expect(view.getTemplateData).to.be.a('function');
        expect(view.template).to.be.a('function');
        expect(view.beforeRender).to.be.a('function');
        expect(view.afterRender).to.be.a('function');
        expect(view.delegate).to.be.a('function');
        expect(view.undelegate).to.be.a('function');
        expect(view.modelBind).to.be.a('function');
        expect(view.modelUnbind).to.be.a('function');
        expect(view.modelUnbindAll).to.be.a('function');
        return expect(view.dispose).to.be.a('function');
      });
      it("should have some default options", function() {
        expect(view.containerMethod).to.be.a('string');
        expect(view.container).not.to.exist;
        expect(view.filter).to.be.a('boolean');
        expect(view.subViewsByName).not.to.exist;
        expect(view.subViews).not.to.exist;
        expect(view.hasSubView).to.be.a('boolean');
        return expect(view.autoRender).to.be.a('boolean');
      });
      it("should have some default value", function() {
        expect(view.containerMethod).to.equal('append');
        expect(view.hasSubView).to.equal(false);
        expect(view.autoRender).to.equal(false);
        return expect(view.filter).to.equal(false);
      });
      it("should have unsubscribeEvent function", function() {
        return expect(view.unsubscribeEvent).to.be.a('function');
      });
      return it("should mixing subscriber", function() {
        var name, value, _results;
        _results = [];
        for (name in Subscriber) {
          if (!__hasProp.call(Subscriber, name)) continue;
          value = Subscriber[name];
          _results.push(expect(view[name]).to.equal(Subscriber[name]));
        }
        return _results;
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/controller_test": function(exports, require, module) {
  (function() {
    var Controller, Subscriber,
      __hasProp = Object.prototype.hasOwnProperty;

    Controller = require('lib/controllers/controller');

    Subscriber = require('lib/subscriber');

    describe("[LIB] Controller", function() {
      var cont;
      cont = null;
      beforeEach(function() {
        return cont = new Controller;
      });
      afterEach(function() {
        cont.dispose();
        return cont = null;
      });
      it("should be exported object", function() {
        return expect(_.isObject(Controller)).to.equal(true);
      });
      it("should mixing subscriber", function() {
        var name, value, _results;
        _results = [];
        for (name in Subscriber) {
          if (!__hasProp.call(Subscriber, name)) continue;
          value = Subscriber[name];
          _results.push(expect(cont[name]).to.equal(Subscriber[name]));
        }
        return _results;
      });
      it("should have a disposed function dispose", function() {
        expect(cont.disposed).to.equal(false);
        return expect(cont.dispose).to.be.a('function');
      });
      return it("should be have the ability to dispose itself", function() {
        cont.dispose();
        return cont.disposed.should.be.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/mediator_test": function(exports, require, module) {
  (function() {
    var mediator;

    mediator = require('lib/mediator');

    describe("[Lib] Mediator Test", function() {
      it("should be defined", function() {
        expect(_.isObject(mediator)).to.equal(true);
        return expect(mediator).to.exist;
      });
      it("should have subscribe, unsubscribe, and publish functions", function() {
        expect(mediator.subscribe).to.be.a('function');
        expect(mediator.unsubscribe).to.be.a('function');
        return expect(mediator.publish).to.be.a('function');
      });
      it("should have supports for channels", function() {
        return expect(mediator.channels).to.not.be.undefined;
      });
      return describe("Publisher Test", function() {
        var eventName, payload, spy;
        eventName = 'foo';
        payload = 'payload';
        spy = null;
        beforeEach(function() {
          return spy = sinon.spy();
        });
        afterEach(function() {
          return delete spy;
        });
        it('should publish messages to subscribers', function() {
          spy.should.not.have.been.called;
          mediator.subscribe(eventName, spy);
          mediator.publish(eventName);
          return spy.should.have.been.calledOnce;
        });
        it("should not do callback once unsubscribed", function() {
          mediator.subscribe(eventName, spy);
          mediator.unsubscribe(eventName, spy);
          mediator.publish(eventName);
          return spy.should.not.have.been.called;
        });
        it("should unsubscribe all if callback arguments is not given", function() {
          var spy2;
          spy2 = sinon.spy();
          mediator.subscribe(eventName, spy);
          mediator.subscribe(eventName, spy2);
          mediator.unsubscribe(eventName);
          mediator.publish(eventName);
          spy.should.not.have.been.called;
          return spy2.should.not.have.been.called;
        });
        return it("should call with payload", function() {
          mediator.subscribe(eventName, spy);
          mediator.publish(eventName, payload);
          spy.should.have.been.calledWith(payload);
          return mediator.unsubscribe(eventName);
        });
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/model_test": function(exports, require, module) {
  (function() {
    var Model, Subscriber,
      __hasProp = Object.prototype.hasOwnProperty;

    Model = require('lib/models/model');

    Subscriber = require('lib/subscriber');

    describe("[LIB] Model", function() {
      var model;
      model = null;
      beforeEach(function() {
        return model = new Model;
      });
      afterEach(function() {});
      it("should be exported object", function() {
        return expect(_.isObject(Model)).to.equal(true);
      });
      return it("should mixing subscriber", function() {
        var name, value, _results;
        _results = [];
        for (name in Subscriber) {
          if (!__hasProp.call(Subscriber, name)) continue;
          value = Subscriber[name];
          _results.push(expect(model[name]).to.equal(Subscriber[name]));
        }
        return _results;
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/router_test": function(exports, require, module) {
  (function() {
    var Router;

    Router = require('lib/router');

    describe("[LIB] Router", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(Router)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/subscriber_test": function(exports, require, module) {
  (function() {
    var Subscriber, mediator;

    Subscriber = require('lib/subscriber');

    mediator = require('lib/mediator');

    describe("[LIB] Subscriber", function() {
      it("should be exported object", function() {
        return expect(_.isObject(Subscriber)).to.equal(true);
      });
      it("should have a subscribeEvent function", function() {
        return expect(Subscriber.subscribeEvent).to.be.a('function');
      });
      it("should have unsubscribeEvent function", function() {
        return expect(Subscriber.unsubscribeEvent).to.be.a('function');
      });
      it("should have an unsubscribeAllEvents function", function() {
        return expect(Subscriber.unsubscribeAllEvents).to.be.a('function');
      });
      it("should check for subscriberEvent arguments type and throw error", function() {
        (function() {
          return Subscriber.subscribeEvent(1);
        }).should["throw"]("1st argument should be a string");
        return (function() {
          return Subscriber.subscribeEvent("1", "2");
        }).should["throw"]("2st argument should be a callback function");
      });
      it("should be able subscribe a event", function() {
        var spy;
        spy = sinon.spy();
        Subscriber.subscribeEvent("place", spy);
        spy.should.not.have.been.calledOnce;
        mediator.publish("place");
        return spy.should.have.been.calledOnce;
      });
      it("should check for unsubscriberEvent arguments type and throw error", function() {
        (function() {
          return Subscriber.unsubscribeEvent(1);
        }).should["throw"]("1st argument should be a string");
        return (function() {
          return Subscriber.unsubscribeEvent("1", "2");
        }).should["throw"]("2st argument should be a callback function");
      });
      it("should be able unsubscribe a event", function() {
        var spy;
        spy = sinon.spy();
        Subscriber.subscribeEvent("place", spy);
        spy.should.not.have.been.calledOnce;
        mediator.publish("place");
        spy.should.have.been.calledOnce;
        Subscriber.unsubscribeEvent("place", spy);
        mediator.publish("place");
        return spy.should.not.have.been.calledTwice;
      });
      return it("should be able unsubscribe all events", function() {
        var spy;
        spy = sinon.spy();
        Subscriber.subscribeEvent("place", spy);
        spy.should.not.have.been.calledOnce;
        mediator.publish("place");
        spy.should.have.been.calledOnce;
        Subscriber.unsubscribeAllEvents();
        mediator.publish("place");
        return spy.should.not.have.been.calledTwice;
      });
    });

  }).call(this);
  
}});

window.require.define({"test/lib/view_test": function(exports, require, module) {
  (function() {
    var Subscriber, View,
      __hasProp = Object.prototype.hasOwnProperty;

    View = require('lib/views/view');

    Subscriber = require('lib/subscriber');

    describe("[LIB] View", function() {
      var view;
      view = null;
      beforeEach(function() {
        return view = new View;
      });
      afterEach(function() {
        return view.dispose();
      });
      it("should be defined", function() {
        expect(_.isObject(View)).to.equal(true);
        return expect(view).to.exist;
      });
      it("should have some default function", function() {
        expect(view.render).to.be.a('function');
        expect(view.addSubView).to.be.a('function');
        expect(view.getTemplateData).to.be.a('function');
        expect(view.template).to.be.a('function');
        expect(view.beforeRender).to.be.a('function');
        expect(view.afterRender).to.be.a('function');
        expect(view.delegate).to.be.a('function');
        expect(view.undelegate).to.be.a('function');
        expect(view.modelBind).to.be.a('function');
        expect(view.modelUnbind).to.be.a('function');
        expect(view.modelUnbindAll).to.be.a('function');
        return expect(view.dispose).to.be.a('function');
      });
      it("should have some default options", function() {
        expect(view.containerMethod).to.be.a('string');
        expect(view.container).not.to.exist;
        expect(view.filter).to.be.a('boolean');
        expect(view.subViewsByName).not.to.exist;
        expect(view.subViews).not.to.exist;
        expect(view.hasSubView).to.be.a('boolean');
        return expect(view.autoRender).to.be.a('boolean');
      });
      it("should have some default value", function() {
        expect(view.containerMethod).to.equal('append');
        expect(view.hasSubView).to.equal(false);
        expect(view.autoRender).to.equal(false);
        return expect(view.filter).to.equal(false);
      });
      it("should have unsubscribeEvent function", function() {
        return expect(view.unsubscribeEvent).to.be.a('function');
      });
      return it("should mixing subscriber", function() {
        var name, value, _results;
        _results = [];
        for (name in Subscriber) {
          if (!__hasProp.call(Subscriber, name)) continue;
          value = Subscriber[name];
          _results.push(expect(view[name]).to.equal(Subscriber[name]));
        }
        return _results;
      });
    });

  }).call(this);
  
}});

window.require.define({"test/models/job_test": function(exports, require, module) {
  (function() {
    var Job;

    Job = require("models/job");

    describe("[APP] Job model", function() {
      return it("should be defined", function() {
        return expect(1).to.equal(1);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/test-helpers": function(exports, require, module) {
  (function() {
    var chai, sinonChai;

    chai = require('chai');

    sinonChai = require("sinon-chai");

    chai.use(sinonChai);

    module.exports = {
      expect: chai.expect,
      chai: chai,
      should: chai.should(),
      sinonChai: sinonChai,
      sinon: require('sinon'),
      _: require('underscore')
    };

  }).call(this);
  
}});

window.require.define({"test/views/app_view_test": function(exports, require, module) {
  (function() {
    var AppView;

    AppView = require('views/app_view');

    describe("[APP] AppView", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(AppView)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/views/breadcrumb_view_test": function(exports, require, module) {
  (function() {
    var BreadCrumView;

    BreadCrumView = require('views/breadcrumb_view');

    describe("[APP] BreadCrumView", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(BreadCrumView)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/views/footer_view_test": function(exports, require, module) {
  (function() {
    var FooterView;

    FooterView = require('views/footer_view');

    describe("[APP] FooterView", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(FooterView)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/views/header_view_test": function(exports, require, module) {
  (function() {
    var HeaderView;

    HeaderView = require('views/header_view');

    describe("[APP] HeaderView", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(HeaderView)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require.define({"test/views/home/job_view_test": function(exports, require, module) {
  (function() {



  }).call(this);
  
}});

window.require.define({"test/views/home_page_view_test": function(exports, require, module) {
  (function() {
    var HeaderView;

    HeaderView = require('views/home_page_view');

    describe("[APP] HeaderView", function() {
      return it("should be exported object", function() {
        return expect(_.isObject(HeaderView)).to.equal(true);
      });
    });

  }).call(this);
  
}});

window.require('test/controllers/app_controller_test');
window.require('test/controllers/breadcrum_controller_test');
window.require('test/controllers/footer_controller_test');
window.require('test/controllers/header_controller_test');
window.require('test/controllers/home_page_controller_test');
window.require('test/lib/application_test');
window.require('test/lib/collection_test');
window.require('test/lib/collection_view_test');
window.require('test/lib/controller_test');
window.require('test/lib/mediator_test');
window.require('test/lib/model_test');
window.require('test/lib/router_test');
window.require('test/lib/subscriber_test');
window.require('test/lib/view_test');
window.require('test/models/job_test');
window.require('test/views/app_view_test');
window.require('test/views/breadcrumb_view_test');
window.require('test/views/footer_view_test');
window.require('test/views/header_view_test');
window.require('test/views/home/job_view_test');
window.require('test/views/home_page_view_test');
