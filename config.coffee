exports.config =
	paths:
		public: 'public'

	server :
		path: 'server/server.coffee'
		port: 3333
	# See docs at http://brunch.readthedocs.org/en/latest/config.html.
	files:
		javascripts:
			defaultExtension: 'coffee'
			joinTo:
				'javascripts/app.js': /^app/
				'javascripts/vendor.js': /^vendor/
				'test/javascripts/test.js': /^test(\/|\\)(?!vendor)/
				'test/javascripts/test-vendor.js': /^test(\/|\\)(?=vendor)/
			order:
				before: [
					'vendor/scripts/console-helper.js',
					'vendor/scripts/modernizr.js',
					'vendor/scripts/jquery-1.7.2.js',
					'vendor/scripts/underscore-1.3.3.js',
					'vendor/scripts/backbone-0.9.2.js',
					'vendor/scripts/bootstrap/bootstrap-model.js'
				]

		stylesheets:
			defaultExtension: 'less'
			joinTo: 'stylesheets/app.css'
			order:
				before: ['vendor/styles/bootstrap/bootstrap.css']
				after: ['vendor/styles/helpers.css']

		templates:
			defaultExtension: 'hbs'
			joinTo: 'javascripts/app.js'